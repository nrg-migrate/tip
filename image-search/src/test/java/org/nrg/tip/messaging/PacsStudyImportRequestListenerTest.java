/*
 * org.nrg.tip.messaging.PacsStudyImportRequestListenerTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.messaging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.tip.TipBaseTest;
import org.nrg.tip.dicom.command.cfind.CFindTest;
import org.nrg.tip.domain.entities.Pacs;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class PacsStudyImportRequestListenerTest extends TipBaseTest {

    // trying to write a multithreaded test in a non-nasty manner...and failing

    public static PacsStudyImportRequest messageReceivedByListener;

    @Inject
    private JmsTemplate template;

    @Resource(name = "pacsStudyImportRequest")
    private Destination destination;

    @Resource(name = "dataSource")
    private DataSource _dataSource;

    @Before
    public void before() throws SQLException {
        messageReceivedByListener = null;
        cleanDb();
    }

    public void cleanDb() throws SQLException {
        final Connection connection = _dataSource.getConnection();
        final Statement statement = connection.createStatement();
        statement.execute("DELETE FROM ACTIVEMQ_MSGS;");
        statement.close();
    }

    @Test
    public synchronized void objectMessage() throws JMSException, InterruptedException {
        final PacsStudyImportRequest messageToSend = mockPacsStudyImportRequest();
        template.convertAndSend(destination, messageToSend);
        for (int i = 1; i <= 5 && null == messageReceivedByListener; ++i) {
            Thread.sleep(1000);
        }
        assertNotNull(messageReceivedByListener);
        assertNotSame(messageToSend, messageReceivedByListener);
        assertEquals(messageToSend, messageReceivedByListener);
    }

    private PacsStudyImportRequest mockPacsStudyImportRequest() {
        // I haven't done much with Serialization, so I really want to kick the tires by populating all fields
        final PacsStudyImportRequest pacsStudyImportRequest = new PacsStudyImportRequest();
        pacsStudyImportRequest.setPacs(mockPacs());
        pacsStudyImportRequest.setStudy(CFindTest.mockStudy());
        pacsStudyImportRequest.setDateRequested(new Date());
        pacsStudyImportRequest.setRequestingUser(null);
        pacsStudyImportRequest.addSeries(mockPacsSeriesImportRequest());
        return pacsStudyImportRequest;
    }

    private Pacs mockPacs() {
        final Pacs pacs = new Pacs();
        pacs.setAeTitle("TIP-DEV-PACS");
        pacs.setHost("10.28.16.215");
        pacs.setStoragePort(11112);
        pacs.setQueryRetrievePort(11112);
        pacs.setOrmStrategySpringBeanId("dicomOrmStrategy");
        return pacs;
    }

    private PacsSeriesImportRequest mockPacsSeriesImportRequest() {
        final PacsSeriesImportRequest pacsSeriesImportRequest = new PacsSeriesImportRequest();
        pacsSeriesImportRequest.setSeries(CFindTest.mockSeries());
        return pacsSeriesImportRequest;
    }
}
