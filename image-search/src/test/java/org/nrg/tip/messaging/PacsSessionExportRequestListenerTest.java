/*
 * org.nrg.tip.messaging.PacsSessionExportRequestListenerTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.messaging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.tip.TipBaseTest;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class PacsSessionExportRequestListenerTest extends TipBaseTest {

    // trying to write a multithreaded test in a non-nasty manner...and failing

    public static PacsSessionExportRequest messageReceivedByListener;

    @Inject
    private JmsTemplate template;

    @Resource(name = "pacsSessionExportRequest")
    private Destination destination;

    @Resource(name = "dataSource")
    private DataSource _dataSource;

    @Before
    public void before() throws SQLException {
	messageReceivedByListener = null;
	cleanDb();
    }

    public void cleanDb() throws SQLException {
	final Connection connection = _dataSource.getConnection();
	final Statement statement = connection.createStatement();
	statement.execute("DELETE FROM ACTIVEMQ_MSGS;");
	statement.close();
    }

    @Test
    public synchronized void objectMessage() throws JMSException, InterruptedException {
	final PacsSessionExportRequest messageToSend = new PacsSessionExportRequest();
	template.convertAndSend(destination, messageToSend);
	for (int i = 1; i <= 5 && null == messageReceivedByListener; ++i) {
	    Thread.sleep(1000);
	}
	assertNotNull(messageReceivedByListener);
	assertNotSame(messageToSend, messageReceivedByListener);
	assertEquals(messageToSend, messageReceivedByListener);
    }
}
