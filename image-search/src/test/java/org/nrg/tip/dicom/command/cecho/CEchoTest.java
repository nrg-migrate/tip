/*
 * org.nrg.tip.dicom.command.cecho.CEchoTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.dicom.command.cecho;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.tip.dicom.command.DicomCommandTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CEchoTest extends DicomCommandTest {

    @Test
    public void cecho() {
	// test will be run by parent class
    }
}
