/*
 * org.nrg.tip.dicom.strategy.orm.PatientNameStrategyTest
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.dicom.strategy.orm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.nrg.tip.domain.PatientName;
import org.nrg.tip.dto.PacsSearchCriteria;

@RunWith(JUnit4.class)
public class PatientNameStrategyTest {

    private PatientNameStrategy strategy;

    @Before
    public void setup() {
	strategy = new BasicPatientNameStrategy();
    }

    @Test
    public void dicomPatientNameToTipPatientNameEmpty() {
	final PatientName pn = strategy.dicomPatientNameToTipPatientName(null);
	assertNull(pn.getFirstName());
	assertNull(pn.getLastName());
    }

    @Test
    public void dicomPatientNameToTipPatientNameLastNameOnly() {
	final PatientName pn = strategy.dicomPatientNameToTipPatientName("Ness");
	assertNull(pn.getFirstName());
	assertEquals("Ness", pn.getLastName());
    }

    @Test
    public void dicomPatientNameToTipPatientNameFirstAndLastName() {
	final PatientName pn = strategy.dicomPatientNameToTipPatientName("Ness^Eliot");
	assertEquals("Eliot", pn.getFirstName());
	assertEquals("Ness", pn.getLastName());
	assertNull(pn.getMiddleName());
	assertNull(pn.getPrefix());
	assertNull(pn.getSuffix());
    }

    @Test
    public void dicomPatientNameToTipPatientNameFullName() {
	final PatientName pn = strategy.dicomPatientNameToTipPatientName("Ness^Eliot^D^Agent^Jr");
	assertEquals("Eliot", pn.getFirstName());
	assertEquals("Ness", pn.getLastName());
	assertEquals("D", pn.getMiddleName());
	assertEquals("Agent", pn.getPrefix());
	assertEquals("Jr", pn.getSuffix());
    }

    private PacsSearchCriteria tipSearchCriteria(String patientName) {
	final PacsSearchCriteria ps = new PacsSearchCriteria();
	ps.setPatientName(patientName);
	return ps;
    }

    private DicomPersonNameSearchCriteria expectedDicomSearchCriteria(String... criteria) {
	final DicomPersonNameSearchCriteria dpnsc = new DicomPersonNameSearchCriteria();
	for (String criterion : criteria) {
	    dpnsc.addCriterion(StringUtils.trimToEmpty(criterion));
	}
	return dpnsc;
    }

    @Test
    public void tipSearchCriterionToDicomSearchCriterionLastName() {
	final PacsSearchCriteria ps = tipSearchCriteria("Ness");
	assertEquals(expectedDicomSearchCriteria("Ness*^^^^"), strategy.tipSearchCriteriaToDicomSearchCriteria(ps));
    }

    @Test
    public void tipSearchCriterionToDicomSearchCriterionLastNamePlusComma() {
	final PacsSearchCriteria ps = tipSearchCriteria("Ness,");
	assertEquals(expectedDicomSearchCriteria("Ness*^^^^"), strategy.tipSearchCriteriaToDicomSearchCriteria(ps));
    }

    @Test
    public void tipSearchCriterionToDicomSearchCriterionLastNameAndFirstInitial() {
	final PacsSearchCriteria ps = tipSearchCriteria("  Ness ,  E   ");
	assertEquals(expectedDicomSearchCriteria("Ness^E", "Ness^E*^^^"),
		strategy.tipSearchCriteriaToDicomSearchCriteria(ps));
    }

    @Test
    public void tipSearchCriterionToDicomSearchCriterionLastNameAndFirstName() {
	final PacsSearchCriteria ps = tipSearchCriteria("Ness, Eliot");
	assertEquals(expectedDicomSearchCriteria("Ness^Eliot", "Ness^Eliot*^^^"),
		strategy.tipSearchCriteriaToDicomSearchCriteria(ps));
    }

    @Test
    public void tipSearchCriterionToDicomSearchCriterionNoComponentsPopulated() {
	final PacsSearchCriteria ps = tipSearchCriteria(null);
	assertEquals(expectedDicomSearchCriteria(""), strategy.tipSearchCriteriaToDicomSearchCriteria(ps));
    }
}
