/*
 * org.nrg.tip.dicom.command.cfind.dcm4che.tool.Dcm4cheToolCFindSCU
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.dicom.command.cfind.dcm4che.tool;

import org.nrg.tip.dicom.command.cecho.CEchoSCU;
import org.nrg.tip.dicom.command.cecho.dcm4che.tool.Dcm4cheToolCEchoSCU;
import org.nrg.tip.dicom.command.cfind.CFindSCU;
import org.nrg.tip.dicom.net.DicomConnectionProperties;
import org.nrg.tip.dicom.strategy.orm.OrmStrategy;
import org.nrg.tip.domain.Patient;
import org.nrg.tip.domain.Series;
import org.nrg.tip.domain.Study;
import org.nrg.tip.dto.PacsSearchCriteria;
import org.nrg.tip.dto.PacsSearchResults;

public class Dcm4cheToolCFindSCU implements CFindSCU {

    private DicomConnectionProperties dicomConnectionProperties;

    private CEchoSCU cechoSCU;

    private OrmStrategy ormStrategy;

    public Dcm4cheToolCFindSCU(final DicomConnectionProperties dicomConnectionProperties, final OrmStrategy ormStrategy) {
        this.dicomConnectionProperties = dicomConnectionProperties;
        cechoSCU = new Dcm4cheToolCEchoSCU(dicomConnectionProperties);
        this.ormStrategy = ormStrategy;
    }

    @Override
    public PacsSearchResults<String, Patient> cfindPatientsByExample(final PacsSearchCriteria searchCriteria) {
        return new CFindSCUPatientLevelByExample(dicomConnectionProperties, cechoSCU, ormStrategy)
                .cfind(searchCriteria);
    }

    @Override
    public Patient cfindPatientById(final String patientId) {
        PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        searchCriteria.setPatientId(patientId);
        PacsSearchResults<String, Patient> searchResults = new CFindSCUPatientLevelById(dicomConnectionProperties,
                cechoSCU, ormStrategy).cfind(searchCriteria);
        return searchResults.getFirstResult();
    }

    @Override
    public PacsSearchResults<String, Study> cfindStudiesByExample(final PacsSearchCriteria searchCriteria) {
        return new CFindSCUStudyLevelByExample(dicomConnectionProperties, cechoSCU, ormStrategy).cfind(searchCriteria);
    }

    @Override
    public Study cfindStudyById(final String studyInstanceUid) {
        PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        searchCriteria.setStudyInstanceUid(studyInstanceUid);
        PacsSearchResults<String, Study> searchResults = new CFindSCUStudyLevelById(dicomConnectionProperties,
                cechoSCU, ormStrategy).cfind(searchCriteria);
        return searchResults.getFirstResult();
    }

    @Override
    public PacsSearchResults<String, Series> cfindSeriesByStudy(final Study study) {
        PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        if (null != study) {
            searchCriteria.setStudyInstanceUid(study.getStudyInstanceUid());
        }
        return new CFindSCUSeriesLevelByStudy(dicomConnectionProperties, cechoSCU, ormStrategy).cfind(searchCriteria);
    }

    @Override
    public Series cfindSeriesById(final String seriesInstanceUid) {
        PacsSearchCriteria searchCriteria = new PacsSearchCriteria();
        searchCriteria.setSeriesInstanceUid(seriesInstanceUid);
        PacsSearchResults<String, Series> searchResults = new CFindSCUSeriesLevelById(dicomConnectionProperties,
                cechoSCU, ormStrategy).cfind(searchCriteria);
        return searchResults.getFirstResult();
    }

    /**
     * Post-construction get made available for unit testing hackage.
     */
    public OrmStrategy getOrmStrategy() {
        return ormStrategy;
    }
}
