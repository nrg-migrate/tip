/*
 * org.nrg.tip.services.MockPacsService
 * TIP is developed by the Neuroinformatics Research Group
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 9/24/13 6:11 PM
 */

package org.nrg.tip.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.nrg.tip.domain.Patient;
import org.nrg.tip.domain.PatientName;
import org.nrg.tip.domain.ReferringPhysicianName;
import org.nrg.tip.domain.Series;
import org.nrg.tip.domain.Study;
import org.nrg.tip.domain.entities.Pacs;
import org.nrg.tip.dto.PacsSearchCriteria;
import org.nrg.tip.dto.PacsSearchResults;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.security.XDATUser;

@SuppressWarnings("unused")
public class MockPacsService implements PacsService {

    @Override
    public PacsSearchResults<String, Patient> getPatientsByExample(final XDATUser user, final Pacs pacs,
                                                                   final PacsSearchCriteria searchCriteria) {
        throw new RuntimeException("method not implemented");
    }

    @Override
    public Patient getPatientById(final XDATUser user, final Pacs pacs, final String patientId) {
        throw new RuntimeException("method not implemented");
    }

    @Override
    public Study getStudyById(final XDATUser user, final Pacs pacs, final String studyInstanceUid) {
        throw new RuntimeException("method not implemented");
    }

    @Override
    public PacsSearchResults<String, Study> getStudiesByExample(final XDATUser user, final Pacs pacs,
                                                                final PacsSearchCriteria searchCriteria) {
        final Patient patient = getMockPatient();
        final Map<String, Study> studies = new HashMap<String, Study>(2);
        studies.put("34525253.34235.23456.1", getMockStudy(patient, "34525253.34235.23456.1"));
        studies.put("65860386.24536543.25922", getMockStudy(patient, "65860386.24536543.25922"));
        return new PacsSearchResults<String, Study>(studies, true, null);
    }

    private Patient getMockPatient() {
        final Patient p = new Patient();
        p.setId("8675309");
        p.setName(new PatientName("Jenny, Jenny"));
        try {
            p.setBirthDate(new SimpleDateFormat("yyyy-MM-dd").parse("1990-01-01"));
        } catch (final ParseException e) {
            throw new RuntimeException(e);
        }
        p.setSex("F");
        return p;
    }

    private Study getMockStudy(final Patient p, final String studyInstanceUid) {
        final Study s = new Study();
        s.setStudyInstanceUid(studyInstanceUid);
        s.setReferringPhysicianName(new ReferringPhysicianName("Tommy", "Tutone"));
        try {
            s.setStudyDate(new SimpleDateFormat("yyyy-MM-dd").parse("2008-01-01"));
        } catch (final ParseException e) {
            throw new RuntimeException(e);
        }
        s.setPatient(p);
        return s;
    }

    @Override
    public PacsSearchResults<String, Series> getSeriesByStudy(final XDATUser user, final Pacs pacs, final Study study) {
        throw new RuntimeException("method not implemented");
    }

    @Override
    public Series getSeriesById(final XDATUser user, final Pacs pacs, final String seriesInstanceUid) {
        throw new RuntimeException("method not implemented");
    }

    @Override
    public void importSeries(final XDATUser user, final Pacs pacs, final Study study, final Series series) {
        throw new RuntimeException("method not implemented");
    }

    @Override
    public void exportSeries(final XDATUser user, final Pacs pacs, final XnatImagescandata series) {
        throw new RuntimeException("method not implemented");
    }
}
